#include <CliArguments.hpp>

#include "tiny-unit.hpp"

class BasicOpts : public tiny::Unit
{
public:
    BasicOpts()
    : tiny::Unit(__FILE__)
    {
         tiny::Unit::registerTest(&BasicOpts::condensedValue, "condensedValue");
		 tiny::Unit::registerTest(&BasicOpts::help, "help");
         tiny::Unit::registerTest(&BasicOpts::version, "version");
         tiny::Unit::registerTest(&BasicOpts::skip, "skip");
         tiny::Unit::registerTest(&BasicOpts::condensedBool, "condensedBool");
    }

	static void condensedValue()
	{
		const char * input[3] = { "unittest", "-i123", "-aabcd" };

		bodhi::CliArguments args(3, input);

		auto i = args.getOpt<int>("i", "", -1);

		TINY_ASSERT_EQUAL(i, 123);
		TINY_ASSERT_OK(i != -1);

		auto a = args.getOpt<std::string>("a", "", "foo");

		TINY_ASSERT_EQUAL(a, "abcd");
		TINY_ASSERT_OK(a != "foo");
	}

    static void help()
    {
        const char * input[2] = { "unittest", "--help" };

        bodhi::CliArguments args(2, input);

        auto help = args.getOpt("h", "help");
        auto other = args.getOpt("a", "");

        TINY_ASSERT_OK(help);
        TINY_ASSERT_OK(not other);
        TINY_ASSERT_EQUAL(args.arguments().size(), 0);
    }

    static void version()
    {
        const char * input[3] = { "unittest", "--version", "foobar" };

        bodhi::CliArguments args(3, input);

        auto version = args.getOpt("", "version");
        auto other = args.getOpt("", "longOpt");

        TINY_ASSERT_OK(version);
        TINY_ASSERT_OK(not other);
        TINY_ASSERT_EQUAL(args.arguments().size(), 1);
        TINY_ASSERT_EQUAL(args.arguments().at(0), "foobar");
    }

    static void skip()
    {
		const char * input[7] = { "unittest", "-a", "1", "--", "foobar", "-b", "2" };

        bodhi::CliArguments args(7, input);

        int a = args.getOpt<int>("a", "", 0);
        int b = args.getOpt<int>("b", "", 0);

        TINY_ASSERT_EQUAL(a, 1);
        TINY_ASSERT_EQUAL(b, 0);

        TINY_ASSERT_EQUAL(args.arguments().size(), 4);
        TINY_ASSERT_EQUAL(args.arguments().at(0), "--");
        TINY_ASSERT_EQUAL(args.arguments().at(1), "foobar");
        TINY_ASSERT_EQUAL(args.arguments().at(2), "-b");
        TINY_ASSERT_EQUAL(args.arguments().at(3), "2");
    }


    static void condensedBool()
    {
        const char * input[3] = { "unittest", "-abc", "-xz" };

        bodhi::CliArguments args(3, input);

        auto x = args.getOpt("x", "");
        auto y = args.getOpt("y", "");
        auto z = args.getOpt("z", "");

        TINY_ASSERT_OK(x);
        TINY_ASSERT_OK(not y);
        TINY_ASSERT_OK(z);
        TINY_ASSERT_EQUAL(args.arguments().size(), 1);
    }
};

BasicOpts basicOpts;
