# CliArguments v1.1.1

Adhering the POSIX and GNU command line parameter standards.

*POSIX*

- Arguments are options if they begin with a hyphen delimiter (‘-’).
- Options are a hyphen followed by a single alphanumeric character: '-o'
- Options may require an argument: '-o argument' or '-oargument'
- Options without arguments are of type bool and true when set
- Options without arguments can be grouped after a hyphen: '-lst' same as '-t -l -s'
- Options can appear in any order: '-lst' same as '-tls'
- The '--' argument terminates options, following arguments are non-options

*GNU*

- Define long name options: '--output' for '-o'
- Always provide '-h', '--help' and '--version' options.

## Installation

This library consists of just one header file. Copy it into your source tree.

You can also use the [CPM}(https://github.com/TheLartians/CPM) in your CMake project.

```cmake
CPMAddPackage(
  NAME cliarguments
  GIT_REPOSITORY https://gitlab.com/aggsol/cliarguments-cpp.git
  GIT_TAG v1.1.1
)
```

## Usage

```cpp
  bodhi::CliArguments args(argc, argv);

  auto name = args.getOpt<std::string>("n", "name", "none");
  auto count = args.getOpt<int>("c", "count", -1);

  auto version = args.getOpt("", "version");
  auto help = args.getOpt("h", "help");
```

## Build

As it is header only the only thing you can build are the unittest and their code coverage in the debug build.

```sh
$ cmake .. -DCLIARG_ENABLE_UNITTEST=True -DCMAKE_BUILD_TYPE=Debug -DCLIARG_ENABLE_COVERAGE=True
```